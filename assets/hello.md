<div id="teachers">
<h3>Teachers</h3></div>

<h4>Rebeckh Burns</h4>

![bek](https://clients.mindbodyonline.com/studios/YogaLimitedBikramYogaBritomart/staff/100000000_large.jpg)

The founder of Hot Yoga Works in Auckland. 

Rebeckh has been teaching Yoga for over 20 years and most recently has added Yoga Barre and Hot HIIT Pilates to her extensive repertoire and experience. 

Rebeckh has authored a best-selling book on Meditation in her 20s, published by Harper Collins Australia, has studied Naturopathy. 

Rebeckh is devoted from her heart to helping people to heal, empower and experience true health and wellbeing through practical techniques of Yoga and beyond with her expert guidance.

<h4>Michal Wolkonski</h4>

![michal](https://i.imgur.com/5LT2abQ.jpg)

 Michal is studio manager and yoga teacher. Since 2010 Michal has practised Hot Yoga Method, gaining his teacher's qualification in Thailand in 2014. Since then he has been teaching hot yoga in the UK, Indonesia and Thailand. He recently moved up from Invercargil.

<br>
<h4>Kate Sproule</h4>

![kate](https://clients-content.mindbodyonline.com/studios/YOGALIMITEDBIKRAMYOGABRITOMART/staff/100000135_mobile.png)

Hi everyone, I’m Kate I’m a committed and passionate Pilates instructor! Raised in beautiful Christchurch, I moved to Auckland to study communications at AUT! My classes are fun, friendly and create a burn! Roll your mat out with me and be challenged, encouraged and supported in one step, one breath and one day at a time. Namaste.

<h4>Kauan Gracie</h4>

![kauan](https://clients-content.mindbodyonline.com/studios/YOGALIMITEDBIKRAMYOGABRITOMART/staff/100000144_mobile.png)

I was born in Rio de Janeiro, Brazil, and have been a mover and athlete for most of my life. I have practiced my family’s martial art Gracie Jiu-Jitsu for as long as I can remember. I have participated in swimming and various other sports before finding a passion in dance. After graduating from Chapman University in Orange County, I moved to Italy to pursue dance before finally settling into a teaching career. Since then, I have taught languages, dance, fitness, female self-defense, kids Jiu-Jitsu, Flexionamento and movement in five different continents. Today I work as a choreographer, teaching workshops and consulting in several of my specialties across the world.

<br>

<div id="reviews">
<h3>Member Reviews</h3></div>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fdana.pau%2Fposts%2F10159085417946320&width=500" width="500" height="259" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D2357790620913973%26id%3D100000491754230%26substory_index%3D0&show_text=true&width=552&height=369&appId" width="552" height="369" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fhotyogaworksbritomart%2Fphotos%2Fa.455859167786901%2F3607857735920346%2F%3Ftype%3D3&width=500" width="500" height="593" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fhotyogaworksbritomart%2Fphotos%2Fa.760783703961111%2F3628974553808664%2F%3Ftype%3D3&show_text=true&width=552&height=705&appId" width="552" height="705" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

<div id="schedule">
<h3>Schedule</h3></div

no upcoming classes	

<div id="pricing">
<h3>Class Prices</h3></div>

Buy Now Online

Intro Pack

14 days of unlimited Hot Yoga, Hot HIIT Pilates and Hot Dance for $49 (Mat included first practise) Please arrive 15 minutes before class to get you all set up

$49 Buy Now

Unlimited $50 per week

Unlimited Yoga, Pilates and Dance $50 per week (Includes 2 towels every practise) $50 Buy Now

Unlimited $40 per week

Unlimited Yoga, Pilates and Dance $40 Buy Now

10 Class Pack

Practise any class, anytime $259 Buy Now

Casual Class

Single Hot Yoga, Hot HIIT Pilates or Hot Dance class $30 Buy Now

Non Refundable | Plenty of parking options nearby, please ask us for more information.


<div id="faq">
<h3>FAQ</h3></faq>

Do you offer parking?

Parking is located at The Chancery Carpark - 8 Bacons Lane, Auckland CBD 1010. Monday to Friday from 5pm to 11:45pm for $4.00. Weekends 7am til 9pm $4.00 for the day.

To book this cheap and convenient parking, visit [Secure Parking’s Secure a Spot service](https://secureparking.co.nz) and use Promo Code: YOGA1 for $4.00 Night Parking and Weekend Parking.

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=174.76388812065127%2C-36.85058226089812%2C174.77210640907288%2C-36.845439530082686&amp;layer=mapnik&amp;marker=-36.848010938730894%2C174.76799726486206" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-36.84801&amp;mlon=174.76800#map=17/-36.84801/174.76800">View Larger Map</a></small>

Parking Monday - Friday before 5pm is located at Downtown Carpark, 31 Custom Street West. 

Purchase a prepaid parking voucher for $4.00 for 2 hours. Limited to one per class.

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=174.76451307535174%2C-36.84505746031673%2C174.76656764745718%2C-36.84377171713848&amp;layer=mapnik&amp;marker=-36.84441459143004%2C174.76554036140442" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-36.84441&amp;mlon=174.76554#map=19/-36.84441/174.76554">View Larger Map</a></small>

<div id="contact">
<h3>Contact</h3></div>

Address: 13 Commerce Street, Auckland City, 1010 

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=174.7654759883881%2C-36.846980668520644%2C174.76958513259888%2C-36.84440922522292&amp;layer=mapnik&amp;marker=-36.84569495768165%2C174.76753056049347" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-36.84569&amp;mlon=174.76753#map=18/-36.84569/174.76753">View Larger Map</a></small>

Phone: [+6493089642](tel:+6493089642)

Email: [info@hotyogaworks.nz](mailto:info@hotyogaworks.nz)

<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fhotyogaworksbritomart%2F&tabs&width=340&height=120&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId" width="340" height="120" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>


